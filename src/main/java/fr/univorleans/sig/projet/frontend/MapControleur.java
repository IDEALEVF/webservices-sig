package fr.univorleans.sig.projet.frontend;


import fr.univorleans.sig.projet.backend.dao.*;
import fr.univorleans.sig.projet.backend.facade.SigService;
import fr.univorleans.sig.projet.backend.modele.appli.Parcvelo;
import fr.univorleans.sig.projet.backend.modele.appli.Pistecyclable;
import fr.univorleans.sig.projet.backend.modele.appli.Stationvelo;
import fr.univorleans.sig.projet.backend.modele.appli.Stationveloplus;
import fr.univorleans.sig.projet.backend.modele.map.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Collection;

@Controller
@RequestMapping("/map")
public class MapControleur {
    @Autowired
    private MaparretRepository maparretRepository;

    @Autowired
    private MapligneRepository mapligneRepository;

    @Autowired
    private MapparcveloRepository mapparcveloRepository;

    @Autowired
    private MappistecyclableRepository mappistecyclableRepository;

    @Autowired
    private MapstationveloRepository mapstationveloRepository;

    @Autowired
    private MapstationveloplusRepository mapstationveloplusRepository;

    @GetMapping(value = "/parcvelo", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Mapparcvelo> getMapParcv(@RequestParam("idParc")int idParc) {
        Mapparcvelo parcvelo = mapparcveloRepository.findByIdparcv(idParc);
        return ResponseEntity.ok().body(parcvelo);
    }

    @GetMapping(value = "/ou-suis", produces={MediaType.APPLICATION_JSON_VALUE})
    public String getPosition(Model model, @RequestParam("long")String longitude, @RequestParam("lat")String lat) {
        model.addAttribute("long",longitude.replace(",","."));
        model.addAttribute("lat",lat.replace(",","."));
        return "sigComplet";
    }

    @GetMapping(value = "/lignes", produces={MediaType.APPLICATION_JSON_VALUE})
    public String getLignes(Model model, @RequestParam("long")String longitude, @RequestParam("lat")String lat) {
        model.addAttribute("long",longitude.replace(",","."));
        model.addAttribute("lat",lat.replace(",","."));
        return "sigTransport";
    }

    @GetMapping(value = "/cyclable", produces={MediaType.APPLICATION_JSON_VALUE})
    public String getPiste(Model model, @RequestParam("long")String longitude, @RequestParam("lat")String lat) {
        model.addAttribute("long",longitude.replace(",","."));
        model.addAttribute("lat",lat.replace(",","."));

        return "sigVelo";
    }

    @GetMapping(value = "/arret", produces={MediaType.APPLICATION_JSON_VALUE})
    public String getMapArret(Model model, @RequestParam("numLigne")String numligne, @RequestParam("idArret")long idarret, @RequestParam("direction")String direction, @RequestParam("long")String longuser, @RequestParam("lat")String latuser) {
        direction = direction.replace("%20"," ");
        Maparret maparret=maparretRepository.findByNumligneAndIdarretAndDirection(numligne, idarret, direction);
        model.addAttribute("long",maparret.getLongitude());
        model.addAttribute("lat",maparret.getLatitude());
        model.addAttribute("longuser",longuser.replace(",","."));
        model.addAttribute("latuser",latuser.replace(",","."));

        return "sigTransport2";
    }

    @GetMapping(value = "/parc", produces={MediaType.APPLICATION_JSON_VALUE})
    public String getParc(Model model, @RequestParam("id")int idParc, @RequestParam("long")String longuser, @RequestParam("lat")String latuser) {
        Mapparcvelo mapparcvelo=mapparcveloRepository.findByIdparcv(idParc);
        model.addAttribute("longuser",longuser.replace(",","."));
        model.addAttribute("latuser",latuser.replace(",","."));
        model.addAttribute("long",mapparcvelo.getLongitude());
        model.addAttribute("lat",mapparcvelo.getLatitude());

        return "sigVelo2";
    }

    @GetMapping(value = "/velop", produces={MediaType.APPLICATION_JSON_VALUE})
    public String getVelop(Model model, @RequestParam("id")int idstationplus, @RequestParam("long")String longuser, @RequestParam("lat")String latuser) {
        Mapstationveloplus mapstationveloplus=mapstationveloplusRepository.findByIdstationplus(idstationplus);
        model.addAttribute("longuser",longuser.replace(",","."));
        model.addAttribute("latuser",latuser.replace(",","."));
        model.addAttribute("long",mapstationveloplus.getLongitude());
        model.addAttribute("lat",mapstationveloplus.getLatitude());

        return "sigVelo2";
    }

}
