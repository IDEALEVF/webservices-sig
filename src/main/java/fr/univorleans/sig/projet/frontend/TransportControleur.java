package fr.univorleans.sig.projet.frontend;

import fr.univorleans.sig.projet.backend.dao.ArretRepository;
import fr.univorleans.sig.projet.backend.dao.LigneRepository;
import fr.univorleans.sig.projet.backend.modele.appli.Arret;
import fr.univorleans.sig.projet.backend.modele.appli.Ligne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
@RequestMapping("/transport")
public class TransportControleur {

    @Autowired
    private LigneRepository ligneRepository;

    @Autowired
    private ArretRepository arretRepository;


    //lignes

    @RequestMapping(value = "/ligneMap", method = RequestMethod.GET, params = {"numLigne"})
    public String ligneMap(@RequestParam(value = "numLigne",required = true)String numLigne) {
        String transport = "transport/%s";
        return  String.format(transport,numLigne) ;
    }

    @GetMapping(value = "/ligne", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Ligne> getLigne(@RequestParam("numLigne")String numLigne) {
        Ligne transport = ligneRepository.findByNumligne(numLigne);
        return ResponseEntity.ok().body(transport);
    }

    @GetMapping(value = "/lignes", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Ligne>> getAllLigne() {
        Collection<Ligne> lignes=ligneRepository.findAll();
        return ResponseEntity.ok().body(lignes);
    }

    @GetMapping(value = "/arret/lignes", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Ligne>> getLigneByArret(@RequestParam("idArret")Long idArret) {
        Arret a=arretRepository.findByIdarret(idArret);
        Collection<Ligne> lignes=ligneRepository.findAllByArrets(a);
        return ResponseEntity.ok().body(lignes);
    }

    //arrets
    @GetMapping(value = "/arret", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Arret> getArret(@RequestParam("idArret")Long idArret) {
        Arret transport = arretRepository.findByIdarret(idArret);
        return ResponseEntity.ok().body(transport);
    }

    @GetMapping(value = "/arrets", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Arret>> getAllArret() {
        Collection<Arret> arrets=arretRepository.findAll();
        return ResponseEntity.ok().body(arrets);
    }

    @GetMapping(value = "/ligne/arrets", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Arret>> getArretByLigne(@RequestParam("numLigne")String numLigne) {
        Ligne l=ligneRepository.findByNumligne(numLigne);
        Collection<Arret> arrets=arretRepository.findAllByLignes(l);
        return ResponseEntity.ok().body(arrets);
    }

    @GetMapping(value = "/nomarret", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Arret> getArretByNom(@RequestParam("nomArret")String nomArret) {
        Arret arrets = arretRepository.findByNomarret(nomArret);
        return ResponseEntity.ok().body(arrets);
    }


}
