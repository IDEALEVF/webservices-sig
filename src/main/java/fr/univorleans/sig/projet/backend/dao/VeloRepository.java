package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Parcvelo;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface VeloRepository extends CrudRepository<Parcvelo, Long> {
    public Collection<Parcvelo> findAll();
    public Parcvelo findByNomarretparc(String nomarretparc);
    public Parcvelo findByIdparcv(Long idparcv);

}
