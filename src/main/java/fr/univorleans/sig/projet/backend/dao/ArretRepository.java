package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Arret;
import fr.univorleans.sig.projet.backend.modele.appli.Ligne;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface ArretRepository extends CrudRepository<Arret, Long> {

    public Arret findByIdarret(long idarret);
    public Collection<Arret> findAll();
    public Collection<Arret> findAllByLignes(Ligne numligne);
    public Arret findByNomarret(String nomarret);

}
