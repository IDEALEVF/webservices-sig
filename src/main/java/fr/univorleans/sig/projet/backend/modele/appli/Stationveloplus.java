package fr.univorleans.sig.projet.backend.modele.appli;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Stationveloplus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idstationplus;
    @NotNull
    private String commune;
    @NotNull
    private String nomarret;
    @NotNull
    private String adressestation;
    private int totalvelos;

    @OneToMany( fetch = FetchType.EAGER, mappedBy = "stationveloplus")
    private Collection<Alerteveloplus> alerteveloplus;

    public Stationveloplus() {
        this.alerteveloplus = new ArrayList<Alerteveloplus>();
    }

    public long getIdstationplus() {
        return idstationplus;
    }

    public void setIdstationplus(long idstationplus) {
        this.idstationplus = idstationplus;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getNomarret() {
        return nomarret;
    }

    public void setNomarret(String nomarret) {
        this.nomarret = nomarret;
    }

    public String getAdressestation() {
        return adressestation;
    }

    public void setAdressestation(String adressestation) {
        this.adressestation = adressestation;
    }

    public int getTotalvelos() {
        return totalvelos;
    }

    public void setTotalvelos(int totalvelos) {
        this.totalvelos = totalvelos;
    }

    public Collection<Alerteveloplus> getAlerteveloplus() {
        return alerteveloplus;
    }

    public void setAlerteveloplus(Collection<Alerteveloplus> alerteveloplus) {
        this.alerteveloplus = alerteveloplus;
    }
    public void addAlerteveloplus(Alerteveloplus alerteveloplus) {
        alerteveloplus.setStationveloplus(this);
        this.alerteveloplus.add(alerteveloplus);
    }
}
