package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Alertearret;
import fr.univorleans.sig.projet.backend.modele.map.Maparret;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface MaparretRepository extends CrudRepository<Maparret, Integer> {

    public Collection<Maparret> findByIdarret(long idarret);
    public Maparret findByNumligneAndIdarretAndDirection(String numligne, long idarret, String direction);

}
