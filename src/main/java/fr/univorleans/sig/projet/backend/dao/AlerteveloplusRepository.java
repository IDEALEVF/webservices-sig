package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Alertearret;
import fr.univorleans.sig.projet.backend.modele.appli.Alerteveloplus;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface AlerteveloplusRepository extends CrudRepository<Alerteveloplus, Long> {

    public Collection<Alerteveloplus> findAll();

}
