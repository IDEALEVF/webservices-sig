package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.map.Maparret;
import fr.univorleans.sig.projet.backend.modele.map.Mapstationvelo;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface MapstationveloRepository extends CrudRepository<Mapstationvelo, Integer> {

    public Mapstationvelo findByIdstation(int idstation);

}
