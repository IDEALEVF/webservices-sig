package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Stationvelo;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface StationveloRepository extends CrudRepository<Stationvelo, Long> {
    public Collection<Stationvelo> findAll();
    public Stationvelo findByIdstation(long idstation);

}
