package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Alertearret;
import fr.univorleans.sig.projet.backend.modele.appli.Alertestationvelo;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface AlertestationveloRepository extends CrudRepository<Alertestationvelo, Long> {

    public Collection<Alertestationvelo> findAll();

}
