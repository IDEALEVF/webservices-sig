package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.map.Maparret;
import fr.univorleans.sig.projet.backend.modele.map.Mappistecyclable;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface MappistecyclableRepository extends CrudRepository<Mappistecyclable, Integer> {

    Mappistecyclable findByIdpiste(int idpiste);

}
