package fr.univorleans.sig.projet.backend.modele.map;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.geo.Point;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;

@Entity
public class Mapparcvelo {
    @Id
    private int idparcv;
    @NotNull
    private int zoom;
    @NotNull
    @Column(name="long")
    private float longitude;
    @NotNull
    @Column(name="lat")
    private float latitude;

    public int getIdparcv() {
        return idparcv;
    }

    public void setIdparcv(int idparcv) {
        this.idparcv = idparcv;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}
